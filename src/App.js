import { Component, Fragment } from 'react';
import './App.css';
import { Card } from "./components/card"
import { Modal } from "./components/modal"

class App extends Component {
  constructor() {
    super();

    this.state = {
      rockets: []
    }
  }

  componentDidMount() {
    fetch("https://api.spacexdata.com/v4/rockets")
    .then((response) => response.json())
    .then(rockets => this.setState({rockets: rockets}))
  }



  render() {
  return (
    
    <div className="container">
      <h1 className="text-center text-white">SpaceX Rockets</h1>
      <div className="row">
      {this.state.rockets.map((rocket) => (
        <Fragment>
       <Card rocket={rocket} />
       <Modal rocket={rocket} />
    
</Fragment>
      ))}
      </div>
    </div>
  );
 }
}
export default App;

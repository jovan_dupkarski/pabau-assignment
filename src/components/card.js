export const Card = ({rocket}) => (
    <div className="col-sm-6">
    <div className="card my-3 text-center " key={rocket.id}>
<img className="card-img-top" src={rocket.flickr_images[1]} alt="Card image cap"/>
<div className="card-body">
<h5 className="card-title">{rocket.name}</h5>
<p className="card-text">Click on "More Info" to find more information about <span className="h5">{rocket.name}</span></p>
<a href="#" className="btn btn-success" data-bs-toggle="modal" data-bs-target={`#popup${rocket.id}`}>More Info</a>
</div>
</div>
</div>
);